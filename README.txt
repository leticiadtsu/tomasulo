Trabalho desenvolvido pelos alunos:
-----------------------------------
André Chateaubriand
Letícia Diniz Tsuchiya
Marco Tulio Costa
-----------------------------------
Agosto/2017

Para executar o programa, basta abrir o arquivo "index.html" e o simulador do Tomasulo será executado no seu navegador padrão. É necessário que o navegador tenha suporte à HTML 5, CSS e JavaScript.

O trabalho foi desenvolvido e testado inicialmente nas versões
- Versão 60.0.3112.101 do Google Chrome
- Versão 55.0.2 do Mozilla Firefox